"""App for managing Eve Online structures with Alliance Auth."""

# pylint: disable = invalid-name
default_app_config = "structures.apps.StructuresConfig"

__version__ = "2.17.0"
__title__ = "Structures"
